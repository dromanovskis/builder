package eu.enifs.mason;

public class MasonryException extends RuntimeException {
    public MasonryException(MethodParameter parameter, Class material, Class expectedType, Class receivedType) {
        super(String.format(
                "Tried to craft (%s) in (%s). Expected parameter type (%s), actual (%s)",
                parameter.getName(),
                material.getTypeName(),
                expectedType.getTypeName(),
                receivedType.getTypeName()));
    }
}
