package eu.enifs.mason;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static java.util.Map.entry;

public class Mason<T> {
    Class<T> clazz;

    Map<Class, Class> primitiveWrappers =
            Map.ofEntries(
                    entry(Byte.class, byte.class),
                    entry(Short.class, short.class),
                    entry(Integer.class, int.class),
                    entry(Long.class, long.class),
                    entry(Float.class, float.class),
                    entry(Double.class, double.class),
                    entry(Boolean.class, boolean.class),
                    entry(Character.class, char.class));

    Map<MethodParameter, Object> parameters = new HashMap<>();

    public Mason(Class<T> clazz) {
        this.clazz = clazz;
    }

    private T createNewInstance() {
        try {
            return clazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException |
                IllegalAccessException |
                InvocationTargetException |
                NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> Mason<T> go(Class<T> clazz) {
        return new Mason<T>(clazz);
    }

    public <V> Mason<T> with(MethodParameter parameter, V value) {
        if (value != null &&
                !primitiveWrappers.containsKey(value.getClass()) &&
                !parameter.getClazz().isAssignableFrom(value.getClass())) {
            throw new MasonryException(parameter, clazz, parameter.getClazz(), value.getClass());
        }

        parameters.put(parameter, value);
        return this;
    }

    private void applyParameter(T instance, MethodParameter parameter, Object value) {
        String setter = methodName(parameter);
        try {
            if (value == null) {
                // in case of null value we just stand aside and let natural ways of declared no args constructor work
                return;
            } else if (primitiveWrappers.containsKey(value.getClass())) {
                try {
                    clazz.getMethod(setter, primitiveWrappers.get(value.getClass()))
                            .invoke(instance, value);
                } catch (NoSuchMethodException e) {
                    clazz.getMethod(setter.toString(), value.getClass()).invoke(instance, value);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            } else {
                Method method;
                if (parameter.getClazz().isAssignableFrom(value.getClass())) {
                    method = clazz.getMethod(setter, parameter.getClazz());
                } else {
                    throw new MasonryException(parameter, clazz, parameter.getClazz(), value.getClass());
                }

                method.invoke(instance, value);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private T resolveParameterMap(T instance) {
        parameters.forEach((parameter, o) -> applyParameter(instance, parameter, o));
        return instance;
    }

    private String methodName(MethodParameter parameter) {
        StringBuilder string = new StringBuilder("set");
        string.append(parameter.getName().substring(0, 1).toUpperCase());
        string.append(parameter.getName().substring(1));
        return string.toString();
    }

    public T create() {
        return resolveParameterMap(createNewInstance());
    }
}