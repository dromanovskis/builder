package eu.enifs.mason;

public interface MethodParameter {
    String getName();
    Class getClazz();
}
