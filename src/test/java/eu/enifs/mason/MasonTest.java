package eu.enifs.mason;

import eu.enifs.mason.model.Animal;
import eu.enifs.mason.model.Cat;
import eu.enifs.mason.model.LonelyCat;
import eu.enifs.mason.model.PrimitiveAnimal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MasonTest {
    @Test
    public void shouldSetParentFieldsCorrectly() {
        Animal animal = Mason.go(Animal.class)
                .with(Animal.fields.furColour, "red")
                .with(Animal.fields.hasWings, true).create();
        Assertions.assertSame("red", animal.getFurColour());
        Assertions.assertSame(true, animal.isHasWings());
    }

    @Test
    public void shouldSetChildProperties() {
        Cat cat = Mason.go(Cat.class).with(Cat.fields.lives, 9).create();
        Assertions.assertSame(9, cat.getLives());
    }

    @Test
    public void shouldSetPropertiesForHierarchy() {
        Cat cat = Mason.go(Cat.class)
                .with(Cat.fields.lives, 9)
                .with(Cat.fields.meows, true)
                .with(Animal.fields.furColour, "blue")
                .with(Animal.fields.hasWings, false)
                .create();

        Assertions.assertSame(9, cat.getLives());
        Assertions.assertSame(true, cat.isMeows());
        Assertions.assertSame("blue", cat.getFurColour());
        Assertions.assertSame(false, cat.isHasWings());
    }

    @Test
    public void shouldSetPrimitiveSetters() {
        byte b = 5;
        short s = 6;
        int i = 7;
        long l = 8;
        float f = 9f;
        double d = 1.0d;
        boolean bool = true;
        char c = 'a';
        PrimitiveAnimal dog = Mason.go(PrimitiveAnimal.class)
                .with(PrimitiveAnimal.fields.b, b)
                .with(PrimitiveAnimal.fields.s, s)
                .with(PrimitiveAnimal.fields.i, i)
                .with(PrimitiveAnimal.fields.l, l)
                .with(PrimitiveAnimal.fields.f, f)
                .with(PrimitiveAnimal.fields.d, d)
                .with(PrimitiveAnimal.fields.bool, bool)
                .with(PrimitiveAnimal.fields.c, c)
                .create();
        Assertions.assertEquals(b, dog.getB());
        Assertions.assertEquals(s, dog.getS());
        Assertions.assertEquals(i, dog.getI());
        Assertions.assertEquals(l, dog.getL());
        Assertions.assertEquals(f, dog.getF());
        Assertions.assertEquals(d, dog.getD());
        Assertions.assertEquals(bool, dog.isBool());
        Assertions.assertEquals(c, dog.getC());
    }

    @Test
    void expectMasonryException(){
        PrimitiveAnimal primitiveAnimal = Mason.go(PrimitiveAnimal.class).create();

        Exception exception = assertThrows(MasonryException.class, () -> {
            Mason.go(LonelyCat.class).with(LonelyCat.fields.companion, primitiveAnimal);
        });

        String expectedMessage = "Tried to craft";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldAcceptSubClasses(){
        Cat cat = Mason.go(Cat.class).create();
        LonelyCat lonelyCat = Mason.go(LonelyCat.class).with(LonelyCat.fields.enemy, cat).create();
        Assertions.assertSame(cat, lonelyCat.getEnemy());
    }

    @Test
    void shouldHandleNullValue(){
         LonelyCat lonelyCat = Mason.go(LonelyCat.class).with(LonelyCat.fields.enemy, null).create();
         Assertions.assertNull(lonelyCat.getEnemy());
    }
}
