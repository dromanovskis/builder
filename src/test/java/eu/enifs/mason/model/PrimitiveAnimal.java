package eu.enifs.mason.model;

import eu.enifs.mason.MethodParameter;

public class PrimitiveAnimal {
    byte b;
    short s;
    int i;
    long l;
    float f;
    double d;
    boolean bool;
    char c;

    public enum fields implements MethodParameter {
        b("b", byte.class),
        s("s", short.class),
        i("i", int.class),
        l("l", long.class),
        f("f", float.class),
        d("d", double.class),
        bool("bool", boolean.class),
        c("c", char.class);

        private String name;
        private Class clazz;

        fields(String name, Class clazz) {
            this.name = name;
            this.clazz = clazz;
        }
        public String getName() {
            return name;
        }

        public Class getClazz() {
            return clazz;
        }
    }

    public byte getB() {
        return b;
    }

    public void setB(byte b) {
        this.b = b;
    }

    public short getS() {
        return s;
    }

    public void setS(short s) {
        this.s = s;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public long getL() {
        return l;
    }

    public void setL(long l) {
        this.l = l;
    }

    public float getF() {
        return f;
    }

    public void setF(float f) {
        this.f = f;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public char getC() {
        return c;
    }

    public void setC(char c) {
        this.c = c;
    }
}
