package eu.enifs.mason.model;

import eu.enifs.mason.MethodParameter;

public class Animal {
    private String furColour;
    boolean hasWings;

    public enum fields implements MethodParameter {
        furColour("furColour", String.class),
        hasWings("hasWings", boolean.class);
        private String name;
        private Class clazz;

        fields(String name, Class clazz) {
            this.name = name;
            this.clazz = clazz;
        }
        public String getName() {
            return name;
        }

        public Class getClazz() {
            return clazz;
        }
    }

    public String getFurColour() {
        return furColour;
    }

    public void setFurColour(String furColour) {
        this.furColour = furColour;
    }

    public boolean isHasWings() {
        return hasWings;
    }

    public void setHasWings(Boolean hasWings) {
        this.hasWings = hasWings;
    }
}
