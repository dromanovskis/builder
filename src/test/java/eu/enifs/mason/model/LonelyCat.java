package eu.enifs.mason.model;

import eu.enifs.mason.MethodParameter;

public class LonelyCat extends Cat {
    private Cat companion;
    private Animal enemy;
    public enum fields implements MethodParameter {
        companion("companion", Cat.class),
        enemy("enemy", Animal.class);

        private String name;
        private Class clazz;

        fields(String name, Class clazz) {
            this.name = name;
            this.clazz = clazz;
        }
        public String getName() {
            return name;
        }

        public Class getClazz() {
            return clazz;
        }
    }

    public Cat getCompanion() {
        return companion;
    }

    public void setCompanion(Cat companion) {
        this.companion = companion;
    }

    public Animal getEnemy() {
        return enemy;
    }

    public void setEnemy(Animal enemy) {
        this.enemy = enemy;
    }
}
