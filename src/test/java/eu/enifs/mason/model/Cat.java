package eu.enifs.mason.model;

import eu.enifs.mason.MethodParameter;

public class Cat extends Animal {
    private boolean meows;
    private int lives;

    public enum fields implements MethodParameter {
        meows("meows", boolean.class),
        lives("lives", int.class);

        private String name;
        private Class clazz;

        fields(String name, Class clazz) {
            this.name = name;
            this.clazz = clazz;
        }
        public String getName() {
            return name;
        }

        public Class getClazz() {
            return clazz;
        }
    }

    public boolean isMeows() {
        return meows;
    }

    public void setMeows(Boolean meows) {
        this.meows = meows;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(Integer lives) {
        this.lives = lives;
    }
}
